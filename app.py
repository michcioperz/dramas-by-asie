#!/usr/bin/env python3
from flask import Flask, render_template_string, render_template, url_for
import json, random, argparse

with open('words.json') as wrdfile:
    words = json.loads(wrdfile.read())

templates = []
with open('templates.txt') as tplfile:
    for tplline in tplfile:
        templates.append(tplline)


app = Flask(__name__)

def get_ac1():
    return random.choice(words['ac1'])

def get_activates():
    return random.choice(words['activates'])

def get_adj():
    return random.choice(words['adjective'])

def get_badsoft():
    return random.choice(words['badsoft'])

def get_ban():
    return random.choice(words['ban'])

def get_code():
    return random.choice(words['code'])

def get_crash():
    return random.choice(words['crash'])

def get_drama():
    return random.choice(words['drama'])

def get_enormous():
    return random.choice(words['enormous'])

def get_function():
    return random.choice(words['function'])

def get_packs():
    return random.choice(words['pack'])

def get_people():
    return random.choice(words['people'])

def get_price():
    return random.choice(words['price'])

def get_says():
    return random.choice(words['says'])

def get_sites():
    return random.choice(words['site'])

def get_things():
    return random.choice(words['thing'])

def get_worse():
    return random.choice(words['worse'])

@app.route('/txt')
def dramatise():
    return render_template_string(random.choice(templates), people=get_people, things=get_things, sites=get_sites, enormous=get_enormous, packs=get_packs, function=get_function, crash=get_crash, price=get_price, adj=get_adj, says=get_says, ac1=get_ac1, drama=get_drama, badsoft=get_badsoft, activates=get_activates, ban=get_ban, code=get_code, worse=get_worse)

@app.route('/')
def homepage():
    return render_template("home.html", dramatise=dramatise, repeat=url_for('homepage'))

def run(host, port, debug=False):
    app.run(host=host, port=port, debug=debug)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('port', type=int)
    parser.add_argument('--host', type=str, default='0.0.0.0')
    parser.add_argument('--debug', '-d', action='store_true')
    args = parser.parse_args()
    run(args.host, args.port, args.debug)
